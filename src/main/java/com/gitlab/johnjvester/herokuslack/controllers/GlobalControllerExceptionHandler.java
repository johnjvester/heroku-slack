package com.gitlab.johnjvester.herokuslack.controllers;

import com.gitlab.johnjvester.herokuslack.utils.RollbarException;
import com.rollbar.notifier.Rollbar;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RequiredArgsConstructor
@ControllerAdvice
public class GlobalControllerExceptionHandler {
    private final Rollbar rollbar;

    @ExceptionHandler(value = Exception.class)
    public void handleExceptions(HttpServletResponse response, Exception e) {
        log.error("e.getMessage()={}", e.getMessage());
        rollbar.error(e.getMessage());

        response.setStatus(HttpStatus.BAD_REQUEST.value());
    }
}

