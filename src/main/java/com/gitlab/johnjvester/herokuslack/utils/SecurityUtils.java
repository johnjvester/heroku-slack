package com.gitlab.johnjvester.herokuslack.utils;

import org.apache.commons.lang3.StringUtils;

public final class SecurityUtils {
    private SecurityUtils() { }

    public static String maskCredentialsRevealPrefix(String string, int prefixLength, char maskCharacter) {
        return StringUtils.overlay(string, StringUtils.repeat(maskCharacter, string.length() - prefixLength), prefixLength, string.length());
    }
}

