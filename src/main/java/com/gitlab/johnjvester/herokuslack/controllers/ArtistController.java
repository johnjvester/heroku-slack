package com.gitlab.johnjvester.herokuslack.controllers;

import com.gitlab.johnjvester.herokuslack.models.Artist;
import com.gitlab.johnjvester.herokuslack.services.ArtistService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@CrossOrigin
@Controller
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ArtistController {
    private final ArtistService artistService;

    @GetMapping(value = "/artists")
    public ResponseEntity<List<Artist>> getAllArtists() {
        log.info("getAllArtists()");
        return new ResponseEntity<>(artistService.getAllArtists(), HttpStatus.OK);
    }

    @GetMapping(value = "/artists/{position}")
    public ResponseEntity<Artist> getArtistByPosition(@PathVariable int position) {
        log.info("getArtistByPosition(position={})", position);
        return new ResponseEntity<>(artistService.getArtistByPosition(position), HttpStatus.OK);
    }
}
