# Using Slack With Heroku (`heroku-slack`) Spring Boot Example Application 

[![pipeline status](https://gitlab.com/johnjvester/heroku-slack/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/heroku-slack/commits/master)

> The `heroku-slack` repository is a [Java](https://en.wikipedia.org/wiki/Java_(programming_language)) API service (utilizing the [Spring Boot](https://spring.io/projects/spring-boot) framework) to help illustrate [Slack](https://slack.com) integration with [Heroku](https://www.heroku.com).

## Publications

This repository is related to the following article published on DZone.com:

* https://dzone.com/articles/three-ways-to-integrate-slack-with-heroku

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Using the `heroku-slack` Service

The `heroku-slack` service returns `Artist` object in all available URIs. 
 
The object is quite simple and is displayed below:

```
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Artist {
    private String name;
}
```

## Artist RESTful API

The RESTful URIs which exist in this repository do not utilize an external data source for the `Artist` object.  Instead, the results are hardcoded into the `ArtistService` so that the focus remain on Slack integration with Heroku.

Use of the `GET` `/artists` URI will return a simple list of artists in `JSON` format:

```
[
    {
        "name": "Eric Johnson"
    },
    {
        "name": "Led Zeppelin"
    },
    {
        "name": "Rush"
    },
    {
        "name": "Tool"
    },
    {
        "name": "Toto"
    },
    {
        "name": "Van Halen"
    },
    {
        "name": "Yes"
    }
]
```

Use of the `GET` `/artists/{position}` URL will return a single `Artist` residing at that ordinal position in the list. 

Using the response above, the `/artists/2` will return the third value (zero-based) in the `List<Artist>` in `JSON` format, as shown below:

```
{
    "name": "Rush"
}
```

Specifying a `{position}` value that is larger than the list size (e.g. `/artists/20`) will return a `400 Bad Request` 
response.  This design is actually **intentional** to help illustrate how an unexpected error can be integrated with Slack. 

## Setup & Configuration

There are no configuration changes are required for the `heroku-slack` Spring Boot application to function within Heroku.  Simply pull down this repository and 
use it with a Heroku project (as noted in the ["Destination Heroku"](https://dzone.com/articles/destination-heroku) article) via the simple `git` command:

`git push heroku`

However, to utilize any of the add-ons which can interact with Slack, the instructions below will be required:

* creating a new [Slack Workspace](#newSlackInstance)
* adding [Activity To Go](#activityToGo)
* adding [Coralogix Logging](#coralogix)
* adding [Rollbar](#rollbar)

### <a name="newSlackInstance">Creating a New Slack Workspace</a>

An existing Slack Workspace can be utilized, but if desired a new Slack Workspace can be created.  Rather than repeat the instructions that 
Slack already provides, simply follow the instructions noted below:

[Create a Slack Workspace](https://slack.com/help/articles/206845317-Create-a-Slack-workspace)

For the purposes of this demonstration, the following channels were created:

* `application-errors` - which will be used by the Rollbar add-on product
* `coralogix` - which will be used by the Coralogix Logging add-on product
* `service-alerts` - which will be used by the Activity To Go add-on product

### <a name="activityToGo">Activity To Go</a>

[Activity To Go](https://elements.heroku.com/addons/activitytogo) keeps your team notified on all Heroku app changes and 
integrates quickly and easily with Slack.  In fact, of all the integrations performed in this exercise, Activity To Go is 
by far the easiest to implement.

Adding the Activity To Go add-on can be completed using the Heroku user-interface or the following command:

`heroku addons:create activitytogo:free`

Once installed, the Activity To Go Dashboard is available via the Heroku user-interface:

![Activity To Go Launch Dashboard](./images/ActivityToGoLaunchDashboard.png)

Once launched, the Dashboard appears as shown below:

![Activity To Go Empty Dashboard](./images/ActivityToGoEmptyDashboard.png)

#### Connecting to Slack

For the purposes of this example, Activity To Go will be used to send alerts any time the Heroku Dyno changes.  The use-case 
here is to alert the feature team when changes are being pushed to Heroku.

In order to accomplish this need, the **Add subscription** button is used from the Activity To Go Dashboard.  A new subscription 
form is then opened:

![Activity To Go New Subscription](./images/ActivityToGoNewSubscription.png)

For the purposes of this example, I named my subscription **Service Alerts** and selected the **Release** and **App** options.  Next, 
the **Configure** button for "Send a Slack message" was clicked.  

This popped up a modal to allow access to Slack:

![Activity To Go Access Slack](./images/ActivityToGoAccessSlack.png)

The **service-alerts** Slack channel was selected and the **Allow** button was clicked.

The **Configure Slack action** modal displayed so the settings could be confirmed:

![Activity To Go Configure Slack](./images/ActivityToGoConfigureSlack.png)

Upon pressing the **Done** button, the new subscription is ready to be saved:

![Activity To Go Service Alerts](./images/ActivityToGoServiceAlerts.png)

Once saved, the Activity To Go Dashboard is updated:

![Activity To Go Dashboard](./images/ActivityToGoDashboard.png)


### <a name="coralogix">Coralogix Logging</a>

[Coralogix Logging](https://elements.heroku.com/addons/coralogix) provides logging which is integrated with Heroku's existing status levels.

Adding the Coralogix Logging add-on can be completed using the Heroku user-interface or the following command:

`heroku addons:create coralogix:free-30mbday`

Once installed, the Coralogix Dashboard can be displayed from the Heroku user inteface:

![New Coralogix Dashboard](./images/NewCoralogixDashboard.png)

#### Connecting to Slack

In order to connect to Slack, enter the `Settings` within the Coralogix Dashboard.  Next, select the `Webhooks` option and select the
`Add new webhook` option (using the plus icon on the right side of the screen).  Provide an alias as shown below and make sure to select 
the `Slack` option:

![New Coralogix Slack Webhook](./images/NewCoralogixSlackWebhook.png)

With the Slack instance open, launch the following URL:

[Create a new Slack Webhook](https://my.slack.com/services/new/incoming-webhook)

Select the channel you wish to use (or create a new) and single click the **Add Incoming Webhooks Integration** button:

![Add Incoming Webhook Integration](./images/AddIncomingWebhookIntegration.png)

If the page does not refresh, refresh the page and note the **Webhook URL** value.

![New Webhook URL](./images/WebhookUrl.png)

Copy and paste the value in the Coralogix Webhook form.
 
![Updated Coralogix Integration](./images/UpdatedCoralogixIntegration.png)
 
To validate, single-click the **Test Configuration** button.  A message similar to what 
is display below should appear in the Slack instance:

![Slack Test Response](./images/SlackTestResponse.png)

At this point, Coralogix is now integrated with the specified Slack channel.

#### Configuring Alerts

With Coralogix ready for use, I decided to create two custom alerts:

* Activity Alert
* Out of Bounds Error Alert

##### Activity Alert

Within the Alerts section of the Coralogix dashboard, a new Alert was created with the following attributes to track activity for the API:

![Activity Alert - Screen 1](./images/CreateActivityAlert1.png)

![Activity Alert - Screen 2](./images/CreateActivityAlert2.png)

##### Out of Bounds Error Alert

Within the Alerts section of the Coralogix dashboard, a new Alert was created with the following attributes to track when the Out of Bounds exception occurs:

![Out Of Bounds Error - Screen 1](./images/OutOfBoundsError1.png)

![Out Of Bounds Error - Screen 2](./images/OutOfBoundsError2.png)

![Out Of Bounds Error - Screen 3](./images/OutOfBoundsError3.png)


### <a name="rollbar">Adding Rollbar</a>

[Rollbar](https://elements.heroku.com/addons/rollbar) provides a different approach to application monitoring. 
It's focused on not only agile development and continuous delivery, but on providing real-time visibility into 
your application without having to refresh cluttered log screens and mine mountains of data. Furthermore, the 
data that arrives into the Rollbar dashboard not only delivers on the metrics expected by production support and 
DevOps teams, but also links to the underlying source code — even to the point where existing tickets can be linked 
to an unexpected event ... or a new ticket can be created directly from Rollbar itself.  

For more information, please review the following article published on DZone.com:

[Discovering Rollbar as a Spring Boot Developer](https://dzone.com/articles/discovering-rollbar-as-a-spring-boot-developer)

Adding the Rollbar add-on can be completed using the Heroku user-interface or the following command:

`heroku addons:create rollbar:free`

The `pom.xml` file has already been updated in this repository to include the following dependency:

```
<dependency>
    <groupId>com.rollbar</groupId>
    <artifactId>rollbar-spring-boot-webmvc</artifactId>
    <version>1.7.4</version>
</dependency>
```

Next, the `application.yml` was updated to include the following settings:

```
rollbar:
  access-token: ${ROLLBAR_ACCESS_TOKEN}
  branch: master
  environment: ${ROLLBAR_ENVIRONMENT}
  code-version: ${HEROKU_RELEASE_VERSION}
spring:
  application:
    name: heroku-slack
```

Finally, sending information to `Rollbar` is as easy as this example from the `RollbarEvents`:

```
@RequiredArgsConstructor
@Slf4j
@Component
public class RollbarEvents {
    private final Environment environment;
    private final RollbarConfigurationProperties rollbarConfigurationProperties;
    private final Rollbar rollbar;

    @PostConstruct
    public void postConstruct() {
        log.info("Started {} on port #{} using Rollbar accessToken={} ({})",
                environment.getProperty("spring.application.name"),
                environment.getProperty("server.port"),
                SecurityUtils.maskCredentialsRevealPrefix(rollbarConfigurationProperties.getAccessToken(), 5, '*'),
                rollbarConfigurationProperties.getEnvironment());
        rollbar.info(String.format("Started %s on port #%s using Rollbar accessToken=%s (%s)",
                environment.getProperty("spring.application.name"),
                environment.getProperty("server.port"),
                SecurityUtils.maskCredentialsRevealPrefix(rollbarConfigurationProperties.getAccessToken(), 5, '*'),
                rollbarConfigurationProperties.getEnvironment()));
    }
}
```

Using the instructions in the [Discovering Rollbar as a Spring Boot Developer](https://dzone.com/articles/discovering-rollbar-as-a-spring-boot-developer) article, 
the following configuration items were setup in the Heroku instance:

![Heroku Variables for Rollbar](./images/RollBarHerokuVariables.png)

#### Connecting to Slack

In order to connect to Slack, use the **Settings** menu from the Rollbar application (launched from Heroku).  Once the Rollbar project 
is selected, navigate to the **Integrations** | **Notifications** option.  Simple single click the **Slack** icon in the list of **Available Channels**.

After selecting my existing access token, I selected the **#application-errors** channel and left all other defaults in place.

![Rollbar Connected to Slack](./images/RollbarSlackConnected.png) 

Pushing the **Send Test Notification** button will send a test message to Slack.  Navigating to Slack and the *#application-erors* channel show the following test messaage:

![Rollbar Connected Test](./images/RollbarSlackConnectionTest.png)

## Interacting with Slack

Now that everything is setup and configured, we can now demonstrate how the simple Spring Book service running in Heroku can integrate with Slack.

### Deploying a New Release

Giving feature team members the ability to see when the application is deployed to a Heroku Dyno has a number of benefits. 
With the **Activity To Go** add-on in place, the following message will appear in the **#service-alerts** Slack channel:

![Activity To Go Slack Message](./images/ActivityToGoNewDeploy.png)

### Heroku Dyno Configuration Changes

The **Activity To Go** add-on will also post a message in the **#service-alerts** Slack channel when system variables 
change in the Heroku instance:

![Activity To Go Slack Message Environment Change](./images/ActivityToGoHerokuChange.png)


### Making a Simple Request

In this simple repository, using the `GET` `/artists` URI returns a list of hard-coded artists.  **Coralogix Logging** 
is configured to publish the following Slack message in the **#coralogix** channel:

![Coralogix Info Slack Message](./images/CoralogixInfoSlackMessage.png)


### Generating a Bad Request

When a bad request is created, in this case by requesting an `Artist` for a position value which does not exist, both the 
**Coralogix Logging** and **Rollbar** add-on products provide information to Slack.

Trigging off the `ERROR` alert, **Coralogix Loggins** posts the following message in the **#coralogix** channel in Slack:

![Coralogix Critical Slack Message](./images/CoralogixCriticalSlackMessage.png)

Based upon the Java code updates and the `GlobalControllerExceptionHandler`, **Rollbar** posts the following message in the 
**#application-errors** channel in Slack:

![Rollbar Slack Message](./images/RollbarSlackError.png)

Where the **Rollbar** product provides additional value, is the ability to assign the error to a configured team member - directly 
from the Slack instance:

![Rollbar Slack Message - Assign to User](./images/RollbarSlackErrorWithAssignUser.png)

Please note - the error level can also be updated directly from Slack as well.


## Summary Information

Both **Coralogix Logging** and **Rollbar** provide additional information from their user-interfaces.

The **Coralogix Logging** Alert Summary provides a nice break-down of all of the alerts which have been captured:

![Coralogix Alert Summary](./images/CoralogixAlertSummary.png)

**Coralogix Logging** provides a high-level dashboard as well:

![Updated Coralogix Dashboard](./images/UpdatedCoralogixDashboard.png)

**Rollbar** provides an advanced dashboard which can even link directly to the source code:

![Rollbar Dashboard](./images/RollbarDashboard.png)

The Items view provides a high-level summary of each error as well:

![Rollbar Items](./images/RollbarItems.png)

In the end, the project in Heroku contains the following add-ons.  As you can see, for this demonstration, none of the 
employed items will carry a monthly cost for use: 

![Heroku Add-Ons List](./images/HerokuAddonsList.png)


Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
